#include <iostream>
using namespace std;
class PartAbs
{
    private:
        string x, y;
    public:
        void set(string a, string b){
            x=a;
            y=b;
        }
        void print(){
            cout<<"x= " <<x<<endl;
            cout<<"y= " <<y<<endl;
        }
};
int main(){
    PartAbs obj;
    obj.set("Partial", "Abstraction");
    obj.print();
}

